package com.pentastagiu.enums;

public enum OrderStatus {
	DELIVERED, DECLINED, ACCEPTED, PROCESSING, DELIVERING
}
