package com.pentastagiu.models;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.pentastagiu.enums.OrderStatus;

@Entity
@Table(name="\"order\"")
public class Order {

	@Id
	@Column(name="order_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long orderId;
	
	@Column(name="order_total_price")
	private BigDecimal orderTotalPrice;
	
	@Column(name="user_id")
	private Long user_id;
	
	@Column(name="order_status")
	private OrderStatus orderStatus;
	
	@Column(name="created_time")
	private LocalDateTime createdTime;
	
	@Column(name="delivered_time")
	private LocalDateTime deliveredTime;
}
